import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import {AppComponent} from './app.component';

import {
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule,
    MatGridListModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatSelectModule,
    MatSortModule
} from '@angular/material';
import {MatTableModule} from '@angular/material/table';
import {MatInputModule} from '@angular/material/input';
import {ExtensionDetailComponent} from './components/extension/extension-detail/extension-detail.component';
import {ExtensionSummaryComponent} from './components/extension/extension-summary/extension-summary.component';
import {MenuComponent} from './components/menu/menu.component';
import {ExtensionSummaryDataService} from './services/extension-summary-data/extension-summary-data.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatTabsModule, MatCardModule} from '@angular/material';
import {TopCallsComponent} from './components/top-calls/top-calls/top-calls.component';
import {TopCallsByDurationService} from './services/top-calls-data/top-calls-by-duration.service';
import {ExtensionDetailedDataService} from './services/extension-detailed-data/extension-detailed-data.service';
import {TechnicalComponent} from './components/login/technical/technical.component';
import {LoginMenuComponent} from './components/login/menu/menu.component';

import {LoginTechnicalDataService} from './services/login-technical-data/login-technical-data.service';
import {SimpleDialogComponent} from './components/login/technical/simple-dialog.component';
import {FormsModule} from '@angular/forms';
import {PinCodeComponent} from './components/extension/pin-code/pin-code.component';
import {MenuOptionComponent} from './components/login/menu-option/menu-option.component';
import {PinCodeDataService} from './services/pin-code-data/pin-code-data.service';

@NgModule({
    declarations: [
        AppComponent,
        MenuComponent,
        ExtensionSummaryComponent,
        ExtensionDetailComponent,
        TopCallsComponent,
        TechnicalComponent,
        LoginMenuComponent,
        SimpleDialogComponent,
        PinCodeComponent,
        MenuOptionComponent
    ],
    imports: [
        BrowserModule,
        MatButtonModule,
        MatTableModule,
        MatInputModule,
        MatSelectModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MatTabsModule,
        MatCardModule,
        MatSortModule,
        MatProgressSpinnerModule,
        MatDialogModule,
        MatIconModule,
        MatGridListModule,
        FormsModule,
        MatCheckboxModule
    ],
    entryComponents: [
        SimpleDialogComponent
    ],
    providers: [
        ExtensionSummaryDataService,
        TopCallsByDurationService,
        ExtensionDetailedDataService,
        LoginTechnicalDataService,
        PinCodeDataService
    ],
    bootstrap: [AppComponent]
})

export class AppModule {
}
