import {DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import {Extension} from '../models/Extension';
import {MatSort} from '@angular/material';
import 'rxjs-compat/add/observable/of';
import 'rxjs-compat/add/observable/merge';
import {map} from 'rxjs/operators';

export class ExtensionDetailedDataSource extends DataSource<any> {

    constructor(private extensions: Extension[], private sort: MatSort) {
        super();
    }

    connect(): Observable<any[]> {
        if (this.extensions === null || this.extensions === []) {
            this.extensions = [];

            return Observable.of(this.extensions);
        } else {
            const dataChanges = [
                this.extensions,
                this.sort.sortChange
            ];

            return Observable.merge(...dataChanges).pipe(
                map(() => {
                    const sortedData = this.sortData();
                    return sortedData;
                }));
        }
    }

    sortData(): any[] {
        const sortedData = this.extensions.slice();

        if (!this.sort.active || this.sort.direction === '') {
            return sortedData;
        }

        return sortedData.sort((a, b) => {
            let isAsc = this.sort.direction === 'asc';

            switch (this.sort.active) {
                case 'callDate':
                    return this.compare(a.callDate, b.callDate, isAsc);
                case 'clid':
                    return this.compare(+a.clid, +b.clid, isAsc);
                case 'src':
                    return this.compare(+a.src, +b.src, isAsc);
                case 'destination':
                    return this.compare(+a.destination, +b.destination, isAsc);
                case 'duration':
                    return this.compare(+a.duration, +b.duration, isAsc);
                case 'cost':
                    return this.compare(+a.cost, +b.cost, isAsc);
                default:
                    return 0;
            }
        });
    }

    compare(a, b, isAsc) {
        return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
    }

    disconnect() {
    }
}
