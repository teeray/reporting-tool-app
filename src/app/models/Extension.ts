export class Extension {
    private _callDate: String;
    private _clid: String;
    private _src: String;
    private _duration: String;
    private _id: String;
    private _numOfCalls;
    private _destination;
    private _cost;
    private _cnam;
    private _cnum;


    get callDate(): String {
        return this._callDate;
    }

    set callDate(value: String) {
        this._callDate = value;
    }

    get clid(): String {
        return this._clid;
    }

    set clid(value: String) {
        this._clid = value;
    }

    get src(): String {
        return this._src;
    }

    set src(value: String) {
        this._src = value;
    }

    get duration(): String {
        return this._duration;
    }

    set duration(value: String) {
        this._duration = value;
    }

    get id(): String {
        return this._id;
    }

    set id(value: String) {
        this._id = value;
    }

    get numOfCalls() {
        return this._numOfCalls;
    }

    set numOfCalls(value) {
        this._numOfCalls = value;
    }

    get destination() {
        return this._destination;
    }

    set destination(value) {
        this._destination = value;
    }

    get cost() {
        return this._cost;
    }

    set cost(value) {
        this._cost = value;
    }

    get cnam() {
        return this._cnam;
    }

    set cnam(value) {
        this._cnam = value;
    }

    get cnum() {
        return this._cnum;
    }

    set cnum(value) {
        this._cnum = value;
    }
}
