export class NameAndPin {

    private _name: string;
    private _pinCode: string;

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get pinCode(): string {
        return this._pinCode;
    }

    set pinCode(value: string) {
        this._pinCode = value;
    }

}
