import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopCallsComponent } from './top-calls.component';

describe('TopCallsComponent', () => {
  let component: TopCallsComponent;
  let fixture: ComponentFixture<TopCallsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopCallsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopCallsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
