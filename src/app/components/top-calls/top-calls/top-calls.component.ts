import {Component, OnInit, ViewChild} from '@angular/core';
import {Extension} from '../../../models/Extension';
import {TopCallsDataSource} from '../../../util/TopCallsDataSource';
import {TopCallsByDurationService} from '../../../services/top-calls-data/top-calls-by-duration.service';
import {MatSort} from '@angular/material';

@Component({
    selector: 'app-top-calls',
    templateUrl: './top-calls.component.html',
    styleUrls: ['./top-calls.component.css']
})
export class TopCallsComponent implements OnInit {

    extensions: Extension[] = [];
    displayedColumns = ['callDate', 'clid', 'src', 'destination', 'duration', 'cost'];
    dataSource: TopCallsDataSource;

    @ViewChild(MatSort) sort: MatSort;

    constructor(private topCallsByDurationService: TopCallsByDurationService) {
    }

    ngOnInit() {
    }

    getTopCallsByDuration(howMany: number) {
        this.topCallsByDurationService.getTopCallsByDuration(howMany).subscribe((extensions) => {
            this.extensions = extensions;
        });

        this.initDataSource();
    }

    initDataSource() {
        this.dataSource = new TopCallsDataSource(this.extensions, this.sort);
    }
}
