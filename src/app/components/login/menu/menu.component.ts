import {Component, OnInit, ViewChild} from '@angular/core';
import {log} from 'util';
import {TechnicalComponent} from '../technical/technical.component';

@Component({
    selector: 'app-login-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.css']
})
export class LoginMenuComponent implements OnInit {

    technicalUsers = [
        {username: 'teeray', password: '7473'}
    ];

    generalUsers = [
        {username: 'test', password: '123'}
    ];

    credentialsOk = false;
    loginOption = null;
    inputError = false;
    hideSetupPage = true;
    hidePassword = true;

    constructor() {

    }

    ngOnInit() {
    }

    setLoginOption(loginOption) {
        this.loginOption = loginOption;
        console.log(this.loginOption);
    }

    checkCredentials(username, password, loginOption) {
        console.log(loginOption);
        if (loginOption === 'technical') {
            this.technicalUsers.forEach((user) => {
                if (user.username === username && user.password === password) {
                    this.credentialsOk = true;
                    this.inputError = false;
                }
            });
        } else {
            this.generalUsers.forEach((user) => {
                if (user.username === username && user.password === password) {
                    this.credentialsOk = true;
                    this.inputError = false;
                }
            });
        }

        if (this.credentialsOk === false) {
            this.inputError = true;
        }
    }

    resetLoginPage(hideSetupPage: boolean) {
        this.credentialsOk = false;
        this.hideSetupPage = hideSetupPage;
    }

}
