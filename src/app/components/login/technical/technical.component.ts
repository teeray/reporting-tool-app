import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {LoginTechnicalDataService} from '../../../services/login-technical-data/login-technical-data.service';
import {SimpleDialogComponent} from './simple-dialog.component';
import {MatDialog, MatDialogRef} from '@angular/material';
import {DbUser} from '../../../models/DbUser';
import {NameAndPin} from '../../../models/NameAndPin';

export interface Tile {
    cols: number;
    rows: number;
    text: string;
}

@Component({
    selector: 'app-technical',
    templateUrl: './technical.component.html',
    styleUrls: ['./technical.component.css']
})

export class TechnicalComponent implements OnInit {
    showReport = false;
    hideSetupPage = false;

    @Output() hideSetupPageEvent = new EventEmitter<boolean>();

    // STAGE 2
    // showSpinner = false;
    // isComplete = false;
    //
    // dialogRef: MatDialogRef<SimpleDialogComponent>;

    // inputName = null;
    // inputPinCode = null;

    // names: Tile[] = [
    //     {text: 'Name', cols: 1, rows: 1},
    //     {text: 'Pin Code', cols: 1, rows: 1}
    // ];
    //
    nameAndPins: NameAndPin[] = [];

    constructor(private loginTechnicalService: LoginTechnicalDataService, private dialog: MatDialog) {
    }

    ngOnInit() {
    }

    // STAGE 2
    // async uploadFile(event) {
    //     const file: File = event.target.files[0];
    //
    //     if (file !== null) {
    //         const formData: FormData = new FormData();
    //
    //         formData.append('file', file, file.name);
    //
    //         await this.loginTechnicalService.uploadFileTest(formData)
    //             .then(
    //                 res => this.isComplete = res
    //             );
    //
    //         console.log(this.isComplete);
    //
    //         if (this.isComplete === true) {
    //             this.showSpinner = false;
    //             this.dialogRef = this.dialog.open(SimpleDialogComponent);
    //         }
    //
    //     }
    // }

    // STAGE 2
    // addToList(name, pinCode) {
    //     const nameAndPin = new NameAndPin();
    //
    //     nameAndPin.name = name;
    //     nameAndPin.pinCode = pinCode;
    //
    //     this.nameAndPins.push(nameAndPin);
    //
    //     this.names.push({text: name, cols: 1, rows: 1});
    //     this.names.push({text: pinCode, cols: 1, rows: 1});
    // }

    uploadDetails(username, password) {
        console.log(password);
        const dbUser = new DbUser();

        dbUser.username = username;
        dbUser.password = password;

        console.log(dbUser.username);

        this.loginTechnicalService.uploadDetails(dbUser.toJSON(), JSON.stringify(this.nameAndPins));
    }

    hideSetupPageFunction() {
        this.hideSetupPage = true;
        this.hideSetupPageEvent.next(this.hideSetupPage);
    }

}
