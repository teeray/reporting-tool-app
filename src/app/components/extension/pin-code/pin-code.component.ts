import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {MatSort} from '@angular/material';
import {PinCodeDataSource} from '../../../util/PinCodeDataSource';
import {Extension} from '../../../models/Extension';
import {PinCodeDataService} from '../../../services/pin-code-data/pin-code-data.service';

@Component({
  selector: 'app-pin-code',
  templateUrl: './pin-code.component.html',
  styleUrls: ['./pin-code.component.css']
})
export class PinCodeComponent implements OnInit {

    @Input('allowInternal') allowInternal;

    extensions: Extension[] = [];
    displayedColumns = ['clid', 'src', 'destination', 'duration', 'cost'];
    dataSource: PinCodeDataSource;

    @ViewChild(MatSort) sort: MatSort;

  constructor(private pinCodeDataService: PinCodeDataService) { }

  ngOnInit() {
  }

  getByPinCode(pinCode: string) {
      this.pinCodeDataService.getByPinCode(pinCode, this.allowInternal).subscribe((extensions) => {
          this.extensions = extensions;
      });

      this.initDataSource();
  }

  initDataSource() {
      this.dataSource = new PinCodeDataSource(this.extensions, this.sort);
  }

}
