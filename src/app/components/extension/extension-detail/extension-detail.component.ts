import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ExtensionDetailedDataService} from '../../../services/extension-detailed-data/extension-detailed-data.service';
import {ExtensionDetailedDataSource} from '../../../util/ExtensionDetailedDataSource';
import {Extension} from '../../../models/Extension';
import {MatSort} from '@angular/material';

@Component({
    selector: 'app-extension-detail',
    templateUrl: './extension-detail.component.html',
    styleUrls: ['./extension-detail.component.css']
})
export class ExtensionDetailComponent implements OnInit {

    @Input('allowInternal') allowInternal;

    extensions: Extension[] = [];
    displayedColumns = ['callDate', 'clid', 'src', 'destination', 'duration', 'cost'];
    showTable = false;
    dataSource: ExtensionDetailedDataSource;

    @ViewChild(MatSort) sort: MatSort;

    constructor(private extensionDetailedDataService: ExtensionDetailedDataService) {
    }

    ngOnInit() {
    }

    getBySrc(src: string) {
        this.extensionDetailedDataService.getBySrc(src, this.allowInternal).subscribe((extensions) => {
            this.extensions = extensions;
        });

        this.initDataSource();
    }

    initDataSource() {
        this.dataSource = new ExtensionDetailedDataSource(this.extensions, this.sort);
    }

}
