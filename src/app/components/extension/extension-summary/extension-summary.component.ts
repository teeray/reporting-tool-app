import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {ExtensionSummaryDataService} from '../../../services/extension-summary-data/extension-summary-data.service';
import {Extension} from '../../../models/Extension';
import {ExtensionSummaryDataSource} from '../../../util/ExtensionSummaryDataSource';
import {MatSort} from '@angular/material';

@Component({
    selector: 'app-extension-summary',
    templateUrl: './extension-summary.component.html',
    styleUrls: ['./extension-summary.component.css']
})
export class ExtensionSummaryComponent implements OnInit {

    @Input('allowInternal') allowInternal;

    extensions: Extension[] = [];
    displayedColumns = ['clid', 'src', 'duration', 'cost', 'numOfCalls'];
    dataSource: ExtensionSummaryDataSource;

    @ViewChild(MatSort) sort: MatSort;

    constructor(private extensionSummaryDataService: ExtensionSummaryDataService) {
    }

    ngOnInit() {
        this.initExtensions();
    }

    initExtensions() {
        this.extensionSummaryDataService.getAllExtensions().subscribe((extensions) => {
            this.extensions = extensions;
        });
    }

    initDataSource() {
        this.dataSource = new ExtensionSummaryDataSource(this.extensions, this.sort);
    }
}
