import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {DbUser} from '../../models/DbUser';
import {RequestOptions} from '@angular/http';

@Injectable({
    providedIn: 'root'
})
export class LoginTechnicalDataService {

    constructor(private http: HttpClient) {
    }

    async uploadFileTest(formData: FormData) {
        console.log('test');

        const response = await this.http.post<boolean>('http://localhost:8081/login/technical/file-upload', formData).toPromise();
        return response;
    }

    uploadDetails(dbUser: string, nameAndPins: string) {
        console.log(dbUser);
        console.log(nameAndPins);

        let finalJSON;
        finalJSON = '{ "dbUser": ' + dbUser +
            ', "nameAndPinCode": ' + nameAndPins + '}';
        console.log(finalJSON);

        const headers = new HttpHeaders()
            .set('Content-Type', 'application/json');

        this.http.post('http://localhost:8081/login/updateDbDetails', finalJSON, {headers: headers}).subscribe();
    }
}
