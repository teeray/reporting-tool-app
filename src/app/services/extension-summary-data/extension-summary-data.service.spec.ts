import { TestBed, inject } from '@angular/core/testing';

import { ExtensionSummaryDataService } from './extension-summary-data.service';

describe('ExtensionSummaryDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ExtensionSummaryDataService]
    });
  });

  it('should be created', inject([ExtensionSummaryDataService], (service: ExtensionSummaryDataService) => {
    expect(service).toBeTruthy();
  }));
});
