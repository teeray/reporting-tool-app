import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Extension} from '../../models/Extension';

@Injectable()
export class ExtensionSummaryDataService {

    constructor(private http: HttpClient) {
    }

    getAllExtensions() {
        return this.http.get<Extension[]>('http://localhost:8081/reports/summary');
    }
}
