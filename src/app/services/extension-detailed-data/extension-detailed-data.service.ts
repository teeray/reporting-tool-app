import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Extension} from '../../models/Extension';
import {Observable} from 'rxjs/index';

@Injectable()
export class ExtensionDetailedDataService {

    constructor(private http: HttpClient) {
    }

    getBySrc(src: string, allowInternal: boolean): Observable<Extension[]> {
        return this.http.get<Extension[]>('http://localhost:8081/reports/detailed?src=' + src + '&allowInternal=' + allowInternal);
    }
}
