import { TestBed, inject } from '@angular/core/testing';

import { TopCallsByDurationService } from './top-calls-by-duration.service';

describe('TopCallsByDurationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TopCallsByDurationService]
    });
  });

  it('should be created', inject([TopCallsByDurationService], (service: TopCallsByDurationService) => {
    expect(service).toBeTruthy();
  }));
});
