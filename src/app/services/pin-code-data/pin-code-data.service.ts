import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Extension} from '../../models/Extension';

@Injectable()
export class PinCodeDataService {

  constructor(private http: HttpClient) {
  }

  getByPinCode(pinCode: string, allowInternal: boolean) {
    console.log(pinCode);
    console.log('http://localhost:8081/reports/pincode?pinCcode=' + pinCode + '&allowInternal=' + allowInternal);
    return this.http.get<Extension[]>('http://localhost:8081/reports/pincode?pinCode=' + pinCode + '&allowInternal=' + allowInternal);
  }
}
